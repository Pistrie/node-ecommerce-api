const mongoose = require("mongoose");

const OrderSchema = new mongoose.Schema(
  {
    userId: { type: String, required: true },
    products: [
      {
        productId: {
          type: String
        },
        quantity: {
          type: String,
          default: 1
        }
      }
    ],
    amount: { type: Number, required: true },
    address: { type: Object, required: true }, // type: Object because Stripe returns an object
    status: { type: String, default: "pending" }
  },
  { timestamps: true }
);

module.exports = mongoose.model("Order", OrderSchema);
