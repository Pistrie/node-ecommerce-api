const router = require("express").Router();
const User = require("../models/user");
const crypto = require("crypto-js");
const jwt = require("jsonwebtoken");

// REGISTER
router.post("/register", async (req, res) => {
  const newUser = new User({
    username: req.body.username,
    email: req.body.email,
    password: crypto.AES.encrypt(req.body.password, process.env.PASS_SECRET).toString()
  });

  try {
    const savedUser = await newUser.save();
    res.status(201).json(savedUser);
  } catch (e) {
    res.status(500).json(e);
  }
});

// LOGIN
router.post("/login", async (req, res) => {
  try {
    const user = await User.findOne(
      {
        username: req.body.username
      }
    );

    if (!user) {
      res.status(401).json("Wrong username");
      return;
    }

    const hashedPassword = crypto.AES.decrypt(
      user.password,
      process.env.PASS_SECRET
    );
    const originalPassword = hashedPassword.toString(crypto.enc.Utf8);

    const accessToken = jwt.sign({
        id: user._id,
        isAdmin: user.isAdmin
      }, process.env.JWT_SECRET,
      { expiresIn: "3d" }
    );

    if (originalPassword !== req.body.password) {
      res.status(401).json("Wrong password");
      return;
    }

    // separate password from the user object because we don't want to return the password to the user
    const { password, ...others } = user._doc;

    res.status(200).json({ ...others, accessToken });
  } catch (e) {
    res.status(500).json(e);
  }
});

module.exports = router;
